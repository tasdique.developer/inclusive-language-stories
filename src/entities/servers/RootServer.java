package entities.servers;

public class RootServer implements Server {
    @Override
    public String getDeprecatedName() {
        return "master";
    }

    @Override
    public String getName() {
        return "root";
    }
}
