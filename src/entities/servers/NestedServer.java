package entities.servers;

public class NestedServer implements Server {
    @Override
    public String getDeprecatedName() {
        return "slave";
    }

    @Override
    public String getName() {
        return "child";
    }
}
