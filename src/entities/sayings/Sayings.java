package entities.sayings;

public class Sayings {
    public static String getSimpleSaying(boolean isKnownPerson) {
        return isKnownPerson ? "It should be a piece of cake." : "It should be simple.";
    }
}
