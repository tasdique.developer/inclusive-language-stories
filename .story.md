---
focus: src/actors/EmailWriter.java
---
### Inclusive Language Story
Welcome to coding story about using inclusive language in the workspace.
This story is written to highlight the assumptions some people make when writing emails
and to help them correct their language.

First let's take a look on project structure to understand what is this
business logic about. We have the [EmailWriter](/src/actors/EmailWriter.java) class that helps write an email.

We have the "entities," with the central "human entity" interface being the [Person](/src/entities/humans/Person.java) file.
We have multiple classes called [UnidentifiedPerson](/src/entities/humans/UnidentifiedPerson.java),
[NonBinaryPerson](/src/entities/humans/NonBinaryPerson.java), 
[FemalePerson](/src/entities/humans/FemalePerson.java), 
and [MalePerson](/src/entities/humans/MalePerson.java),
all of which implement Person. They are responsible for highlighting the pronouns we use when writing
an email or similar correspondence in the workplace.

We similarly have the [Server](/src/entities/servers/Server.java) interface.
This is implemented by [RootServer](/src/entities/servers/RootServer.java) and 
[NestedServer](/src/entities/servers/NestedServer.java), which highlight how we label servers.

---
focus: src/actors/EmailWriter.java
---
### Updating our language - pronouns
We see there's an assumption that the developers we're asking for in this email are male, when
in fact the people provided could be any other pronoun. We need to rid ourselves of assumption
and move across the class hierarchy to use the most generic definition; 
label them as Unidentified rather than a presumed Male.

---
focus: src/entities/servers/NestedServer.java
---
### Updating our language - servers
There's a use of the word "slave" when referring to servers that we should change. This concept of "master-slave"
is bound to racial issues dating back to the 19th century, and as such there is no need to
refer to it. In this case, we must use a non-deprecated term such as "child" server.

---
focus: src/entities/sayings/Sayings.java
---
### Updating our language - sayings
Here's another assumption that everyone would understand the phrase "It will be a piece of cake"
when in fact, a non American-English native speaker would not understand this. We need to use
simpler language that clearly defines what we're trying to say so everyone is on the same page;
we must acknowledge that we don't fully know the recipient of the email.